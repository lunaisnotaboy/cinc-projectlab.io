---
comments: false
title: Download
menu: sidebar
weight: 20
---

## Omnibus Installer

Cinc Client, Auditor and Workstation can be programmatically installed using https://omnitruck.cinc.sh/install.sh (Unix-like systems) or https://omnitruck.cinc.sh/install.ps1 (Windows systems) as you would do for an [omnibus install of Chef](https://docs.chef.io/install_omnibus.html). You can find detailed instructions in [the "getting started" section]({{< ref start>}}).

## Download plain packages

You can find [our packages here](http://downloads.cinc.sh).

## Package repositories

We don't currently have repositories for standard package managers like `yum` or `apt` but we're [looking for design proposals](https://gitlab.com/cinc-project/distribution/client/-/issues/8).

## EULA Compliance

The following board shows the versions after which the Cinc Project considers the product releases to be compliant with the [Chef Policy on Trademarks](https://www.chef.io/trademark-policy/).

For Chef's position on 3rd party distribution trademark policy compliance, see [this link](https://github.com/chef/chef-oss-practices/blob/master/distributions/distribution-guidelines.md#can-chef-evaluate-my-distribution-to-make-sure-its-following-the-trademark-guidelines)

As a reminder, the software is provided as-is with no warranties as per section 7 of the [Apache-2.0 license](https://www.apache.org/licenses/LICENSE-2.0), under which both the source code and our distibution are licensed.

_Please update to a compliant version as they become available._

| Product                                                                  | EULA Compliance |
| ------------------------------------------------------------------------ | --------------- |
| [Client](http://downloads.cinc.sh/files/stable/cinc/) (Linux)            | >= 15.6.10      |
| [Client](http://downloads.cinc.sh/files/stable/cinc/) (Windows / MacOS)  | >= 15.9.17      |
| [Auditor](http://downloads.cinc.sh/files/stable/cinc-auditor/)           | >= 4.17.7       |
| [Workstation](http://downloads.cinc.sh/files/stable/cinc-workstation/)   | >= 22.1.745     |
| [Server](http://downloads.cinc.sh/files/stable/cinc-server)              | >= 14.14.1      |
| Packager                                                                 | Not started     |
| Dashboard                                                                | Not started     |

### Other distros

#### Chef Habitat&trade; replacements

Work hasn't started yet, but we intend to release a Cinc Packager product in time. Take a look at the [Biome project](https://biome.sh/en/) while we work on a release.
