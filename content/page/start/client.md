---
title: "Client"
draft: false
date: 2020-04-10T15:31:30-05:00
menu:
  sidebar:
    parent: Getting Started
slug: start/client
weight: 5
---

## Client Installation

{{% gs_omnitruck_install version="17.x" product="Cinc Client" omnitruckParams="-v 17" omnitruckParamsWindows="-version 17" %}}

{{% gs_gem_install gem="chef-bin" %}}

{{% gs_package_install channel="stable" package="cinc" %}}

### Workstation

Cinc Auditor is of course included in the [Cinc Workstation]({{< relref "workstation.md" >}})

### Configuration

Cinc Client gets it configuration from `/etc/cinc/client.rb` on UNIX/Linux/MacOS and `c:\cinc\client.rb` on Windows.

Users migrating from Chef will probably want to retain most of their configuration. All Cinc configurations are compatible with their Chef counterparts so you can simply use a symlink or copy the contents as-is.

## Usage

`cinc-client` and associated tools are fully compatible with `chef-client`. We even include a wrapper for `chef-client` to redirect it to `cinc-client` when called. This should facilitate compatibility with your own automation.

All [upstream documentation](https://docs.chef.io) remains valid, and so are the [classes provided by Chef Software Inc.](https://learn.chef.io)

### About Windows

Due to the way bootstrap works on Windows, `knife bootstrap` must be called with `--msi_url <direct url to MSI>` in order to correctly bootstrap a Windows node to Cinc Client. You'll unfortunately need to locate the correct MSI for your Cinc version by navigating http://downloads.cinc.sh/files/stable/.

We're looking into solutions that provide a similar user experience for Windows users and users of unix-like systems.

{{% gs_come_see_us %}}
