---
title: "Workstation"
draft: false
date: 2020-04-10T15:31:30-05:00
menu:
  sidebar:
    parent: Getting Started
slug: start/workstation
weight: 10
---

## Workstation Install

{{% gs_omnitruck_install version="22.x" product="Cinc Workstation" omnitruckParams="-P cinc-workstation -v 22" omnitruckParamsWindows="-project cinc-workstation -version 22" %}}

{{% gs_package_install channel="stable" package="cinc-workstation" %}}

### Included tools

While Cinc Workstation contains all the same tools as Chef Workstation&trade;, some of them had to be modified for compliance:

- mixlib-install serves Cinc Products
- Chef Infra&trade; --> Cinc Client
  - `chef` --> `cinc`
  - `chef-analyze` --> `cinc-analyze`
  - `chef-apply` --> `cinc-apply`
  - `chef-shell` --> `cinc-shell`
  - `chef-solo` --> `cinc-solo`
  - `chef-zero` --> `cinc-zero`
- Chef Inspec&trade; --> Cinc Auditor
- Chef Workstation App
  - This has been removed from our builds due to the extensive word mark replacements which are required and needed maintained.
- Habitat
  - We have included the Biome `bio` CLI as a replacement to the `hab` CLI. This is currently only included on Linux until Biome provides binaries for Windows and MacOS.

### Configuration

Cinc uses the `~/.cinc-workstation` folder for it's configuration on Unix-like systems and `%USERPROFILE%\.cinc-workstation` on Windows.

Users migrating from Chef will probably want to retain most of their configuration. All Cinc configurations are compatible with their Chef counterparts so you can simply use a symlink or copy the contents as-is.

## Usage

Cinc Workstation's tools are fully compatible with their Chef Workstation counterparts. Many are in fact not modified at all. We include wrappers for Chef Workstation binaries we've renamed to the Cinc binaries. This should facilitate compatibility with your existing automation.

All [upstream documentation](https://docs.chef.io) remains valid, and so are the [classes provided by Chef Software Inc.](https://learn.chef.io)

{{% gs_come_see_us %}}
