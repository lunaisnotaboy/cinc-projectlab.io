---
title: "Server"
draft: false
date: 2022-04-20T09:43:22-0700
menu:
  sidebar:
    parent: Getting Started
slug: start/server
weight: 5
---

## Server Installation

### Install using the install script

As with upstream packages, you can use the install script method described in [upstream documentation](https://docs.chef.io/install_omnibus.html). Simply substitute the URL with https://omnitruck.cinc.sh/install.sh.

For example, to install the latest 14.x of cinc-server on a supported platform:

```console
curl -L https://omnitruck.cinc.sh/install.sh | sudo bash -s -- -P cinc-server -v 14
```

{{% gs_package_install channel="stable" package="cinc-server" %}}

### Configuration

Cinc Server gets it configuration from `/etc/cinc-project/cinc-server.rb`.

Users migrating from Chef will probably want to retain most of their configuration. All Cinc configurations are compatible with their Chef counterparts so you can simply use a symlink or copy the contents as-is.

To setup the server, run:

```console
cinc-server-ctl reconfigure
```

## Usage

`cinc-server-ctl` and associated tools are fully compatible with `chef-server-ctl`. We even include a wrapper for `chef-client` to redirect it to `cinc-client` when called. This should facilitate compatibility with your own automation.

All [upstream documentation](https://docs.chef.io) remains valid, and so are the [classes provided by Chef Software Inc.](https://learn.chef.io)

{{% gs_come_see_us %}}
