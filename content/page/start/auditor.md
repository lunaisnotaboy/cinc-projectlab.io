---
title: "Auditor"
draft: false
date: 2020-04-10T15:31:30-05:00
menu:
  sidebar:
    parent: Getting Started
slug: start/auditor
weight: 15
---

## Auditor Install

{{% gs_omnitruck_install version="4.x" product="Cinc Auditor" omnitruckParams="-P cinc-auditor -v 4" omnitruckParamsWindows="-project cinc-auditor -version 4" %}}

{{% gs_gem_install gem="cinc-auditor-bin" %}}

{{% gs_package_install channel="stable" package="cinc-auditor" %}}

### Workstation

Cinc Auditor is of course included in the [Cinc Workstation]({{< relref "workstation.md" >}})

### Profiles

Cinc Auditor profiles are 100% compatible with their upstream counterparts.

## Usage

`cinc-auditor` and associated tools are fully compatible with `inspec`. We even include a wrapper for `inspec` to redirect it to `cinc-auditor` when called. This should facilitate compatibility with your existing automation.

All [upstream documentation](https://inspec.io/docs) remains valid, and so are the [classes provided by Chef Software Inc.](https://learn.chef.io)

{{% gs_come_see_us %}}
